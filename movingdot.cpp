//Moving dot across the screen.
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>

class Dot {
	public:
		static const int DOT_WIDTH = 20;
	        static const int DOT_HEIGHT = 20;
		static const int DOT_VEL = 10;
		Dot();
		void handleEvent( SDL_Event& e );
		void move( SDL_Rect& wall );
		void move( SDL_Rect& wall );
	private:
		int mPosX, mPosY;
		int mVelX, mVelY;
		SDL_Rect mCollider;
};

bool init();
bool loadmedia();
void close();
bool checkCollision( SDL_Rect a, SDL_Rect b );
